<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:51:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Product
 * 
 * @property int $id
 * @property int $sub_category_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Product extends Eloquent
{
	protected $casts = [
		'sub_category_id' => 'int'
	];

	protected $fillable = [
		'sub_category_id',
		'name'
	];

    public function stocks(){
        return $this->hasMany('App\Models\Stock');
    }
    public function units(){
        return $this->hasMany('App\Models\Unit');
    }
    public function productUnits(){
        return $this->belongsToMany('App\Models\ProductUnit');
    }
}
