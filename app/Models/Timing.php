<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:51:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Timing
 * 
 * @property int $id
 * @property \Carbon\Carbon $time
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Timing extends Eloquent
{
	protected $dates = [
		'time'
	];

	protected $fillable = [
		'time'
	];

    public function orders(){
        return $this->hasMany('App\Models\Order');
    }
}
