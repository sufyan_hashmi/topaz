<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:51:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserRoll
 * 
 * @property int $id
 * @property int $user_id
 * @property int $roll_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class UserRoll extends Eloquent
{
	protected $table = 'user_roll';

	protected $casts = [
		'user_id' => 'int',
		'roll_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'roll_id'
	];

    public function users(){
        return $this->belongsToMany('App\Models\User');
    }

    public function rolls(){
        return $this->belongsToMany('App\Models\Roll');
    }
}
