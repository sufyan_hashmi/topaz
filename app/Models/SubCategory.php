<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:51:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SubCategory
 * 
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SubCategory extends Eloquent
{
	protected $casts = [
		'category_id' => 'int'
	];

	protected $fillable = [
		'category_id',
		'name'
	];

    public function category(){
        return $this->hasOne('App\Models\Category');
    }
    public function products(){
        return $this->hasMany('App\Models\Product');
    }
}
