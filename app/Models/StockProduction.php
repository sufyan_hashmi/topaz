<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:51:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StockProduction
 * 
 * @property int $id
 * @property int $stock_id
 * @property int $production_id
 * @property int $qty
 * @property string $details
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class StockProduction extends Eloquent
{
	protected $table = 'stock_production';

	protected $casts = [
		'stock_id' => 'int',
		'production_id' => 'int',
		'qty' => 'int'
	];

	protected $fillable = [
		'stock_id',
		'production_id',
		'qty',
		'details'
	];

    public function stocks(){
        return $this->belongsToMany('App\Models\Stock');
    }
    public function productions(){
        return $this->belongsToMany('App\Models\Production');
    }
}
