<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:51:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Category
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Category extends Eloquent
{
	protected $fillable = [
		'name'
	];

    public function subCategories(){
        return $this->hasMany('App\Models\SubCategory');
    }

}
