<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:51:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProductUnit
 * 
 * @property int $id
 * @property int $product_id
 * @property int $unit_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class ProductUnit extends Eloquent
{
	protected $table = 'product_unit';

	protected $casts = [
		'product_id' => 'int',
		'unit_id' => 'int'
	];

	protected $fillable = [
		'product_id',
		'unit_id'
	];

    public function products(){
        return $this->belongsToMany('App\Models\Product');
    }

    public function units(){
        return $this->belongsToMany('App\Models\Unit');
    }
}
