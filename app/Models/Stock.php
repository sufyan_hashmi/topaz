<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:51:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Stock
 * 
 * @property int $id
 * @property int $product_id
 * @property int $product_unit_id
 * @property int $qty
 * @property float $unit_cost
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Stock extends Eloquent
{
	protected $casts = [
		'product_id' => 'int',
		'product_unit_id' => 'int',
		'qty' => 'int',
		'unit_cost' => 'float'
	];

	protected $fillable = [
		'product_id',
		'product_unit_id',
		'qty',
		'unit_cost'
	];

    public function sales(){
        return $this->hasMany('App\Models\Sale');
    }

    public function productions(){
        return $this->hasMany('App\Models\Production');
    }

    public function stockProductions(){
        return $this->belongsToMany('App\Models\StockProduction');
    }
}
