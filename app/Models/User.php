<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:51:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $contact
 * @property string $email
 * @property \Carbon\Carbon $email_verified_at
 * @property string $password
 * @property bool $is_active
 * @property string $address
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class User extends Eloquent
{
	protected $casts = [
		'is_active' => 'bool'
	];

	protected $dates = [
		'email_verified_at'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'name',
		'contact',
		'email',
		'email_verified_at',
		'password',
		'is_active',
		'address',
		'remember_token'
	];

	public function orders(){
	    return $this->hasMany('App\Models\Order');
    }
	public function userRolls(){
	    return $this->belongsToMany('App\Models\UserRoll');
    }
	public function rolls(){
	    return $this->hasMany('App\Models\Roll');
    }
}
