<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:51:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Order
 * 
 * @property int $id
 * @property int $user_id
 * @property int $timing_id
 * @property int $consumer
 * @property float $amount_paid
 * @property string $details
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Order extends Eloquent
{
	protected $casts = [
		'user_id' => 'int',
		'timing_id' => 'int',
		'consumer' => 'int',
		'amount_paid' => 'float'
	];

	protected $fillable = [
		'user_id',
		'timing_id',
		'consumer',
		'amount_paid',
		'details'
	];

    public function user(){
        return $this->hasOne('App\Models\User');
    }
    public function timing(){
        return $this->hasOne('App\Models\Timing');
    }
    public function productions(){
        return $this->hasMany('App\Models\Production');
    }
}
