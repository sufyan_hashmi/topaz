<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:51:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Production
 * 
 * @property int $id
 * @property string $name
 * @property float $total_cost
 * @property int $order_id
 * @property string $details
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Production extends Eloquent
{
	protected $casts = [
		'total_cost' => 'float',
		'order_id' => 'int'
	];

	protected $fillable = [
		'name',
		'total_cost',
		'order_id',
		'details'
	];

    public function stockProductions(){
        return $this->belongsToMany('App\Models\StockProduction');
    }

    public function stocks(){
        return $this->hasMany('App\Models\Stock');
    }
    public function order(){
        return $this->hasOne('App\Models\Order');
    }

}
