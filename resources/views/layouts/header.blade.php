


<!-- Mirrored from hencework.com/theme/brunette/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jan 2019 21:19:44 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Lightgallery CSS -->
    <link href="{{asset('../dist/css/lightgallery.css')}}" rel="stylesheet" type="text/css">
   <!-- Morris Charts CSS -->
    <link href="{{asset('../vendors/morris.js/morris.css')}}" rel="stylesheet" type="text/css" />
    <!-- Toggles CSS -->
    <link href="{{asset('../vendors/jquery-toggles/css/toggles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('../vendors/jquery-toggles/css/themes/toggles-light.css')}}" rel="stylesheet" type="text/css">
    <!-- select2 CSS -->
    <link href="{{asset('../vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- Daterangepicker CSS -->
    <link href="{{asset('../vendors/daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css" />

    <!-- Data Table CSS -->
    <link href="{{asset('../vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('../vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Custom CSS -->
    <link href="{{asset('../dist/css/style.css')}}" rel="stylesheet" type="text/css">

</head>