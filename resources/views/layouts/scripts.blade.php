<!-- jQuery -->
<script src="{{asset('../vendors/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{asset('../vendors/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('../vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Owl JavaScript -->
<script src="{{asset('../vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>
<!-- FeatherIcons JavaScript -->
<script src="{{asset('../dist/js/feather.min.js')}}"></script>
<!-- Gallery JavaScript -->
<script src="{{asset('../vendors/lightgallery/dist/js/lightgallery-all.min.js')}}"></script>
<script src="{{asset('../dist/js/froogaloop2.min.js')}}"></script>
<!-- Init JavaScript -->
<script src="{{asset('../dist/js/lightgallery-all.js')}}"></script>
<script src="{{asset('../dist/js/landing-data.js')}}"></script>
<script src="{{asset('../dist/js/init.js')}}"></script>
<script src="{{asset('../dist/js/dashboard5-data.js')}}"></script>
<script src="{{asset('../dist/js/validation-data.js')}}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{asset('../dist/js/jquery.slimscroll.js')}} "></script>
<!-- Fancy Dropdown JS -->
<script src="{{asset('../dist/js/dropdown-bootstrap-extended.js')}}"></script>
<!-- Toggles JavaScript -->
<script src="{{asset('../vendors/jquery-toggles/toggles.min.js')}}"></script>
<script src="{{asset('../dist/js/toggle-data.js')}}"></script>
<!-- Counter Animation JavaScript -->
<script src="{{asset('../vendors/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('../vendors/jquery.counterup/jquery.counterup.min.js')}}"></script>
<!-- Easy pie chart JS -->
<script src="{{asset('../vendors/easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>

<!-- Sparkline JavaScript -->
<script src="{{asset('../vendors/jquery.sparkline/dist/jquery.sparkline.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{asset('../vendors/raphael/raphael.min.js')}}"></script>
<script src="{{asset('../vendors/morris.js/morris.min.js')}}"></script>

<!-- EChartJS JavaScript -->
<script src="{{asset('../vendors/echarts/dist/echarts-en.min.js')}}"></script>

<!-- Peity JavaScript -->
<script src="{{asset('../vendors/peity/jquery.peity.min.js')}}"></script>

<!-- Select2 JavaScript -->
<script src="{{asset('../vendors/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('../dist/js/select2-data.js')}}"></script>

<!-- Bootstrap Tagsinput JavaScript -->
<script src="{{asset('../vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.html')}}"></script>

<!-- Daterangepicker JavaScript -->
<script src="{{asset('../vendors/moment/min/moment.min.js')}}"></script>
<script src="{{asset('../vendors/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('../dist/js/daterangepicker-data.js')}}"></script>
<!-- Data Table JavaScript -->
<script src="{{asset('../vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('../vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{asset('../vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{asset('../vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('../dist/js/dataTables-data.js')}}"></script>