<!DOCTYPE html>
<!--
Template Name: Brunette - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: https://hencework.ticksy.com/
License: You must have a valid license purchased only from themeforest to legally use the template for your project.
-->
@include('layouts.header')
<html lang="en">
<body data-spy="scroll" data-target=".navbar" data-offset="60">
<!-- Preloader -->
<div class="preloader-it">
    <div class="loader-pendulums"></div>
</div>
<!-- /Preloader -->

<!-- HK Wrapper -->
<div class="hk-wrapper hk-vertical-nav hk-icon-nav">

    @include('layouts.top-nevbar')
    @include('layouts.vertical-navbar')

@yield('content')
</div>
@include('layouts.scripts')
</body>


<!-- Mirrored from hencework.com/theme/brunette/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jan 2019 21:27:02 GMT -->
</html>