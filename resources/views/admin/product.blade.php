@extends('layouts.master')

@section('content')

    <!-- Main Content -->
    <div class="hk-pg-wrapper">
        <!-- Breadcrumb -->
        <nav class="hk-breadcrumb" aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-light bg-transparent">
                <li class="breadcrumb-item"><a href="#">Forms</a></li>
                <li class="breadcrumb-item active" aria-current="page">Form Layout</li>
            </ol>
        </nav>
        <!-- /Breadcrumb -->

        <!-- Container -->
        <div class="container">
            <!-- Title -->
            <div class="hk-pg-header">
                <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"><i data-feather="align-left"></i></span></span>Categories Form</h4>
            </div>
            <!-- /Title -->
            <!-- Row -->
            <div class="row">
                <div class="col-xl-6">
                    <section class="hk-sec-wrapper">
                        <div class="row">
                            <div class="col-xl-6">
                                <h5 class="hk-sec-title">Unit List</h5>
                            </div>
                            <div class="col-xl-6">
                                <button type="button" class="btn btn-primary btn-rounded" style="float: right;"  data-toggle="modal" data-target="#product-modal">New Product</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm">
                                <div class="table-wrap">
                                    @include('admin.tables.unit')
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-xl-6">
                    <section class="hk-sec-wrapper">
                        <div class="row">
                            <div class="col-xl-6">
                                <h5 class="hk-sec-title">Product Units List</h5>
                            </div>
                            <div class="col-xl-6">
                                <button type="button" class="btn btn-primary btn-rounded" style="float: right;"  data-toggle="modal" data-target="#product-modal">New Product</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm">
                                <div class="table-wrap">
                                    @include('admin.tables.product-unit')
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <section class="hk-sec-wrapper">
                        <div class="row">
                            <div class="col-xl-6">
                                <h5 class="hk-sec-title">Product List</h5>
                            </div>
                            <div class="col-xl-6">
                                <button type="button" class="btn btn-primary btn-rounded" style="float: right;"  data-toggle="modal" data-target="#product-modal">New Product</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm">
                                <div class="table-wrap">
                                    @include('admin.tables.product')
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </div>
    </div>
    <!-- Modal Product New -->
    @include('admin.modals.product')
@endsection