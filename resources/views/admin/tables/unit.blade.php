<table id="unit" class="table table-hover w-100 display pb-30">
    <thead>
    <tr>
        <th>#</th>
        <th>Product</th>
        <th>Description</th>
        <th>Date</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>Tiger Nixon</td>
        <td>Tiger Nixon</td>
        <td>Tiger Nixon</td>
        <td>
            <ul class="font-icons-wrap">
                <li>
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#product-modal-update">
                        <i class="glyphicon glyphicon-pencil small"></i>
                        <span>update Product</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <i class="glyphicon glyphicon-trash small"></i>
                        <span>delete Product</span>
                    </a>
                </li>
            </ul>
        </td>
    </tr>

    </tbody>
    {{--<tfoot>--}}
    {{--<tr>--}}
    {{--<th>Name</th>--}}
    {{--<th>Date</th>--}}
    {{--</tr>--}}
    {{--</tfoot>--}}
</table>