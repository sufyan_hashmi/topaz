@extends('layouts.master')

@section('content')

    <!-- Main Content -->
    <div class="hk-pg-wrapper">
        <!-- Breadcrumb -->
        <nav class="hk-breadcrumb" aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-light bg-transparent">
                <li class="breadcrumb-item"><a href="#">Forms</a></li>
                <li class="breadcrumb-item active" aria-current="page">Form Layout</li>
            </ol>
        </nav>
        <!-- /Breadcrumb -->

        <!-- Container -->
        <div class="container">
            <!-- Title -->
            <div class="hk-pg-header">
                <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"><i data-feather="align-left"></i></span></span>Categories Form</h4>
            </div>
            <!-- /Title -->
            <!-- Row -->
            <div class="row">
                <div class="col-xl-6">
                    <section class="hk-sec-wrapper">
                        <div class="row">
                            <div class="col-xl-6">
                                <h5 class="hk-sec-title">Category List</h5>
                            </div>
                            <div class="col-xl-6">
                                <button type="button" class="btn btn-primary btn-rounded" style="float: right;"  data-toggle="modal" data-target="#category-modal">New Category</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm">
                                <div class="table-wrap">
                                    <table id="datable_1" class="table table-hover w-100 display pb-30">
                                        <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Tiger Nixon</td>
                                            <td>
                                                <ul class="font-icons-wrap">
                                                    <li>
                                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#category-modal-update">
                                                            <i class="glyphicon glyphicon-pencil small"></i>
                                                            <span>update category</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                            <i class="glyphicon glyphicon-trash small"></i>
                                                            <span>delete category</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>

                                        </tbody>
                                        {{--<tfoot>--}}
                                        {{--<tr>--}}
                                            {{--<th>Name</th>--}}
                                            {{--<th>Date</th>--}}
                                        {{--</tr>--}}
                                        {{--</tfoot>--}}
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-xl-6">
                    <section class="hk-sec-wrapper">
                        <div class="row">
                            <div class="col-xl-6">
                                <h5 class="hk-sec-title">Sub-Category List</h5>
                            </div>
                            <div class="col-xl-6">
                                <button class="btn btn-primary btn-rounded" style="float: right;" data-toggle="modal" data-target="#sub-category-modal">New Sub-category</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm">
                                <div class="table-wrap">
                                    <table id="datable_2" class="table table-hover w-100 display pb-30">
                                        <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Sub-Category</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Tiger Nixon</td>
                                            <td>Tiger Nixon</td>
                                            <td>
                                                <ul class="font-icons-wrap">
                                                    <li>
                                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#sub-category-modal-update">
                                                            <i class="glyphicon glyphicon-pencil small"></i>
                                                            <span>update category</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                            <i class="glyphicon glyphicon-trash small"></i>
                                                            <span>delete category</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>

                                        </tbody>
                                        {{--<tfoot>--}}
                                        {{--<tr>--}}
                                            {{--<th>Name</th>--}}
                                            {{--<th>Date</th>--}}
                                        {{--</tr>--}}
                                        {{--</tfoot>--}}
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </div>
    </div>
    <!-- Modal Category New -->
    <div class="modal fade" id="category-modal" tabindex="-1" role="dialog" aria-labelledby="category-modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="category-modalLabel">Create Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="needs-validation" id="addCategory" novalidate>
                    {{ csrf_token() }}
                    <div class="modal-body">

                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-row">
                                            <label for="validationTooltip01">First name</label>
                                            <input type="text" class="form-control" id="validationTooltip01" placeholder="First name" name="name" value="" required>
                                            <div class="valid-tooltip">
                                                Looks good!
                                            </div>
                                            <div class="invalid-tooltip">
                                                Please choose a unique and valid username.
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" onclick="return submit_form('addCategory' , 'addCategory')">Save changes</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <!-- Modal Category Update -->
    <div class="modal fade" id="category-modal-update" tabindex="-1" role="dialog" aria-labelledby="category-modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="category-modalLabel">Update Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="needs-validation" id="updateCategory" novalidate>

                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm">
                                <div class="form-row">
                                    <label for="validationTooltip01">First name</label>
                                    {{ csrf_token() }}
                                    <input type="text" class="form-control" id="validationTooltip01" placeholder="First name" value="" required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                    <div class="invalid-tooltip">
                                        Please choose a unique and valid username.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Sub-Category New -->
    <div class="modal fade" id="sub-category-modal" tabindex="-1" role="dialog" aria-labelledby="category-modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="category-modalLabel">Create Sub-Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="needs-validation" id="addSubCategory" novalidate>
                    {{ csrf_token() }}
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm">
                                <div class="form-row">
                                    <div class="col-md-12 mb-10">
                                        <label for="validationTooltip01">First name</label>
                                        <input type="text" class="form-control" id="validationTooltip01" placeholder="First name" value="" required>
                                        <div class="valid-tooltip">
                                            Looks good!
                                        </div>
                                        <div class="invalid-tooltip">
                                            Please choose a unique and valid username.
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-10">
                                        <label for="country">Country</label>
                                        <select class="form-control custom-select d-block w-100" id="country">
                                            <option value="">Choose...</option>
                                            <option>United States</option>
                                        </select>
                                        <div class="valid-tooltip">
                                            Looks good!
                                        </div>
                                        <div class="invalid-tooltip">
                                            Please choose a unique and valid username.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Sub-Category Update -->
    <div class="modal fade" id="sub-category-modal-update" tabindex="-1" role="dialog" aria-labelledby="category-modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="category-modalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="needs-validation" id="updateSubCategory" novalidate>
                    {{ csrf_token() }}
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm">
                                <div class="form-row">
                                    <label for="validationTooltip01">First name</label>
                                    <input type="text" class="form-control" id="validationTooltip01" placeholder="First name" value="" required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                    <div class="invalid-tooltip">
                                        Please choose a unique and valid username.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script>
    // $(document).ready(function() {
        function submit_form(form_id , url) {
            console.log('helo');
            // var data=$('#' +form_id ).serialize();
            var data=$('#addCategory' ).serialize();
            var url2 = '/'+ url;
            console.log(data);

            $.ajax({
                url: '/addcategory',
                type: 'POST',
                data: data,
                // dataType: 'JSON',
                success: function(data){
                    console.log(data);
                },
                error : function(jqXHR, textStatus, errorThrown){
                    alert('error');
                }
            });


            return false;
        }
    // });
</script>
@endsection