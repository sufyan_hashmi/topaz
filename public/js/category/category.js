

function submit_form(form_id ,url){
    console.lop(form_id);
}

$('body').on('submit','#evaluationForm',function(e){

    e.preventDefault();
    var data=$(this).serialize();
    console.log(data);
    // alert('hello');

    $.ajax({
        url: window.location.protocol + "//" + window.location.host+"/hydra/public/evaluation-submit",
        type: 'POST',
        data: data,
        dataType: 'JSON',
        success: function(data){
            console.log(data);

            if(data.marks > 0 ) {
                var id = $("#evaluated_applicant_id").val();
                $('#marks' + id).html('<p class="text-muted">'+data.marks+'</p>');
                $('#evaluation' + id).html('<p class="text-muted"><span class="badge badge-pill badge-success">Evaluated</span></p>');
            }else{
                $('#marks' + id).html('<p class="text-muted">0</p>');
            }
            var drop ='';
            drop+='<select class="form-control" name="applicant_id" id="evaluated_applicant_id" required>';
            drop+='<option selected="">Choose...</option>';
            $.each(data.not_evaluated , function (index , value) {
                drop+='<option value="'+value.id+'">'+value.name+'</option>';
            });
            drop+=' </select>';
            // console.log(drop);
            $('#new_evaluation_dropdown').html(drop);

            $('#addEvaluation').on('hidden.bs.modal', function(){
                $(this).find('form')[0].reset();
            });
            $('#addEvaluation').modal('hide');
            // $("#offerLetter").on("hidden.bs.modal", function(){
            //     $(this).removeData();
            // });

        },
        error : function(jqXHR, textStatus, errorThrown){
            alert('error');
        }
    });
});